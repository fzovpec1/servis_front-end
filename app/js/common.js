var sign_in     = document.getElementById('sign-in'),
    sign_up     = document.getElementById('sign-up'),
    sign_up_btn = document.getElementById('sign-up__btn'),
    sign_in_btn = document.getElementById('sign-in__btn'),
    isOpened    = false;

sign_in_btn.onclick = function(){
    sign_in.style.display = 'block';
    isOpened = true;
}
sign_up_btn.onclick = function(){
    sign_up.style.display = 'block';
    isOpened = true;
}
document.onclick = function(e){
    if(e.target.id !== 'sign-in' && e.target.id !== 'sign-in__btn'){
        sign_in.style.display = 'none';
        isOpened = false;
    }
    if(e.target.id !== 'sign-up' && e.target.id !== 'sign-up__btn'){
        sign_up.style.display = 'none';
        isOpened = false;
    }
}
